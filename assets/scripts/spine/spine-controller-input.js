// reference:
// [keyboard keys]
// http://www.cocos2d-x.org/docs/api-ref/creator/v1.9/en/enums/KEY.html

cc.Class({
    extends: cc.Component,

    properties: {
      spine_controller: {
  			default: null,
        //type: cc.Component,
  			type: require('./spine-controller'),
  		},
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
      //add keyboard input listener to call turnLeft and turnRight
      cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
      cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }, // onLoad

    start () {

    },

    update (dt) {
      //cc.log("[spine-controller] update STARTS");
      if (null !== this.spine_controller && undefined !== this.spine_controller) {
        // TODO: these commands here is just a test of concept
        if(this.actionA) {
          cc.log("[spine-controller] update: this.actionA");
          this.spine_controller.shoot ();
        }
        else if(this.directionLeft) {
          cc.log("[spine-controller] update: this.directionLeft");
          this.spine_controller.walk ();
        }
        else if(this.directionRight) {
          cc.log("[spine-controller] update: this.directionRight");
          this.spine_controller.run ();
        }
        else if(this.directionUp) {
          cc.log("[spine-controller] update: this.directionUp");
          this.spine_controller.jump ();
        }
        else if(this.directionDown) {
          cc.log("[spine-controller] update: this.directionDown");
          this.spine_controller.stop ();
        }

        if(this.function_one) {
          cc.log("[spine-controller] update: this.function_one");
          this.spine_controller.toggleDebugSlots ();
        }
        if(this.function_two) {
          cc.log("[spine-controller] update: this.function_two");
          this.spine_controller.toggleDebugBones ();
        }
        if(this.function_three) {
          cc.log("[spine-controller] update: this.function_three");
          this.spine_controller.toggleTimeScale ();
        }
      }
      //cc.log("[spine-controller] update ENDS");
    }, // update


    // input handler
  	onKeyDown (event) {
          switch(event.keyCode) {
              case cc.KEY.a:
              case cc.KEY.left:
                  //cc.log('left direction button pressed');
                  this.directionLeft = true;
                  break;
              case cc.KEY.d:
              case cc.KEY.right:
                  //cc.log('right direction button pressed');
                  this.directionRight = true;
                  break;

              case cc.KEY.w:
              case cc.KEY.up:
                  //cc.log('up direction button pressed');
                  this.directionUp = true;
                  break;
              case cc.KEY.s:
              case cc.KEY.down:
                  //cc.log('down direction button pressed');
                  this.directionDown = true;
                  break;

              case cc.KEY.f1:
                  //cc.log('function one button pressed');
                  this.function_one = true;
                  break;
              case cc.KEY.f2:
                  //cc.log('function two button pressed');
                  this.function_two = true;
                  break;
              case cc.KEY.f3:
                  //cc.log('function three button pressed');
                  this.function_three = true;
                  break;

  			case cc.KEY.space:
  				//cc.log('space button pressed');
  				this.actionA = true;
  				break;
          }
      }, // onKeyDown

  	onKeyUp (event) {
          switch(event.keyCode) {
              case cc.KEY.a:
              case cc.KEY.left:
                  //cc.log('left direction button released');
                  this.directionLeft = false;
                  break;
              case cc.KEY.d:
              case cc.KEY.right:
                  //cc.log('right direction button released');
                  this.directionRight = false;
                  break;

              case cc.KEY.w:
              case cc.KEY.up:
                  //cc.log('up direction button released');
                  this.directionUp = false;
                  break;
              case cc.KEY.s:
              case cc.KEY.down:
                  //cc.log('down direction button released');
                  this.directionDown = false;
                  break;

              case cc.KEY.f1:
                  //cc.log('function one button released');
                  this.function_one = false;
                  break;
              case cc.KEY.f2:
                  //cc.log('function two button released');
                  this.function_two = false;
                  break;
              case cc.KEY.f3:
                  //cc.log('function three button released');
                  this.function_three = false;
                  break;

  			case cc.KEY.space:
  				//cc.log('space button released');
  				this.actionA = false;
  				break;
          }


      }, // onKeyUp
});
