
// reference:
// http://docs.cocos.com/creator/manual/en/scripting/reference/class.html#type

cc.Class({
  extends: cc.Component,

  properties: {
    movement_controller: {
			default: null,
      //type: cc.Component,
			type: require('./spine-movement-controller'),
		},
  }, // properties

	// button pressed state
	directionLeft:false,
	directionRight:false,
	directionUp:false,
	directionDown:false,

  // LIFE-CYCLE CALLBACKS:

  onLoad: function () {
		//add keyboard input listener to call turnLeft and turnRight
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
		cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

    var visibleSize = cc.director.getVisibleSize();
    var visibleOrigin = cc.director.getVisibleOrigin();
	}, // onLoad

	onDestroy: function () {
    cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
		cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
  }, // onDestroy

  start: function () {
    // unit controller script is no longer a MUST, this is for testing purpose
  	//cc.assert(this.unit_controller, "Invalid unit controller");
}, // start

	// input handler
	onKeyDown (event) {
    switch(event.keyCode) {
      case cc.KEY.a:
      case cc.KEY.left:
          //cc.log('left direction button pressed');
          this.directionLeft = true;
          break;
      case cc.KEY.d:
      case cc.KEY.right:
          //cc.log('right direction button pressed');
          this.directionRight = true;
          break;

      case cc.KEY.w:
      case cc.KEY.up:
          //cc.log('up direction button pressed');
          this.directionUp = true;
          break;
      case cc.KEY.s:
      case cc.KEY.down:
          //cc.log('down direction button pressed');
          this.directionDown = true;
          break;

  		case cc.KEY.space:
  			//cc.log('space button pressed');
  			this.actionA = true;
  			break;
    }
  }, // onKeyDown

	onKeyUp (event) {
    switch(event.keyCode) {
      case cc.KEY.a:
      case cc.KEY.left:
          //cc.log('left direction button released');
          this.directionLeft = false;
          break;
      case cc.KEY.d:
      case cc.KEY.right:
          //cc.log('right direction button released');
          this.directionRight = false;
          break;

      case cc.KEY.w:
      case cc.KEY.up:
          //cc.log('up direction button released');
          this.directionUp = false;
          break;
      case cc.KEY.s:
      case cc.KEY.down:
          //cc.log('down direction button released');
          this.directionDown = false;
          break;

  		case cc.KEY.space:
  			//cc.log('space button released');
  			this.actionA = false;
  			break;
    }
  }, // onKeyUp

  update (dt) {
    if(null !== this.movement_controller) {
      this.movement_controller.onInputUp (this.directionUp);
      this.movement_controller.onInputDown (this.directionDown);

      this.movement_controller.onInputLeft (this.directionLeft);
      this.movement_controller.onInputRight (this.directionRight);

      // var typeCheck = (typeof this.movement_controller === 'MovementController');
      // cc.log("[spine-player-controller] onKeyUp typeCheck: " + typeCheck);
      // cc.log("[spine-player-controller] onKeyUp object type: " + typeof this.movement_controller);
      // cc.log("[spine-player-controller] onKeyUp this.movement_controller.input_rotation_left: " + this.movement_controller.input_rotation_left);
      // cc.log("[spine-player-controller] onKeyUp this.movement_controller.input_rotation_right: " + this.movement_controller.input_rotation_right);
      // cc.log("[spine-player-controller] onKeyUp this.movement_controller.input_move_forward: " + this.movement_controller.input_move_forward);
      // cc.log("[spine-player-controller] onKeyUp this.movement_controller.input_move_backward: " + this.movement_controller.input_move_backward);
    }
  }, // update
});
