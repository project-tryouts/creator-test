cc.Class({
    extends: cc.Component,

    editor: {
        requireComponent: sp.Skeleton
    },

    properties: {
        mixTime: 0.2
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
      var spine = this.spine = this.getComponent('sp.Skeleton');
      this._setMix('walk', 'run');
      this._setMix('run', 'jump');
      this._setMix('walk', 'jump');

      spine.setStartListener(trackEntry => {
          var animationName = trackEntry.animation ? trackEntry.animation.name : "";
          cc.log("[track %s][animation %s] start.", trackEntry.trackIndex, animationName);

          // set the animation state started
          this.state[animationName] = 1;
      });
      spine.setInterruptListener(trackEntry => {
          var animationName = trackEntry.animation ? trackEntry.animation.name : "";
          cc.log("[track %s][animation %s] interrupt.", trackEntry.trackIndex, animationName);

          // set the animation state ended, even if it is interrupted
          this.state[animationName] = 0;
      });
      spine.setEndListener(trackEntry => {
          var animationName = trackEntry.animation ? trackEntry.animation.name : "";
          cc.log("[track %s][animation %s] end.", trackEntry.trackIndex, animationName);

          // set the animation state ended
          this.state[animationName] = 0;
      });
      spine.setDisposeListener(trackEntry => {
          var animationName = trackEntry.animation ? trackEntry.animation.name : "";
          cc.log("[track %s][animation %s] will be disposed.", trackEntry.trackIndex, animationName);
      });
      spine.setCompleteListener((trackEntry, loopCount) => {
          var animationName = trackEntry.animation ? trackEntry.animation.name : "";
          if (animationName === 'shoot') {
              this.spine.clearTrack(1);
          }
          cc.log("[track %s][animation %s] complete: %s", trackEntry.trackIndex, animationName, loopCount);

          // set the animation state complete
          this.state[animationName] = 0;
      });
      spine.setEventListener((trackEntry, event) => {
          var animationName = trackEntry.animation ? trackEntry.animation.name : "";
          cc.log("[track %s][animation %s] event: %s, %s, %s, %s", trackEntry.trackIndex, animationName, event.data.name, event.intValue, event.floatValue, event.stringValue);
      });

      this._hasStop = false;

      // var self = this;
      // cc.eventManager.addListener({
      //     event: cc.EventListener.TOUCH_ALL_AT_ONCE,
      //     onTouchesBegan () {
      //         self.toggleTimeScale();
      //     }
      // }, this.node);

      // define an object
      this.state = {};

      //////////////////////////////////////////////////////////////////////////
      this.node.on("state_moved_forward", this.StateChange_MoveForward, this);
      this.node.on("state_moved_backward", this.StateChange_MoveBackward, this);
      this.node.on("state_idle", this.StateChange_Idle, this);
    }, // onLoad

    StateChange_Idle: function (event) {
       console.log("[StateChange_Idle] event heard");
      // console.log(event);

      var stateName = "StateChange_Idle";
      if (undefined === this.currentState || stateName !== this.currentState) {
        console.log(this.currentState + " >> " + stateName);
          this.currentState = stateName;
          // play the animation based on the state
          this.stop ();
      }
    }, // StateChange_MoveForward

    StateChange_MoveForward: function (event) {
       console.log("[StateChange_MoveForward] event heard");
      // console.log(event);

      // switch the facing direction
      this.node.scaleX = Math.abs(this.node.scaleX);
      console.log("this.node.scaleX: " + this.node.scaleX);

      var stateName = "StateChange_MoveForward";
      if (undefined === this.currentState || stateName !== this.currentState) {
        console.log(this.currentState + " >> " + stateName);
          this.currentState = stateName;
          // play the animation based on the state

          switch (this.currentState) {
            case "StateChange_MoveForward":
            case "StateChange_MoveBackward":
              this.walk ();
            break;
            default:
              this.stop ();
            break;
          }
      }
    }, // StateChange_MoveForward

    StateChange_MoveBackward: function (event) {
       console.log("[StateChange_MoveBackward] event heard");
      // console.log(event);

      // switch the facing direction
      this.node.scaleX = Math.abs(this.node.scaleX) * -1;
      console.log("this.node.scaleX: " + this.node.scaleX);

      var stateName = "StateChange_MoveBackward";
      if (undefined === this.currentState || stateName !== this.currentState) {
        console.log(this.currentState + " >> " + stateName);
          this.currentState = stateName;
          // play the animation based on the state

          switch (this.currentState) {
            case "StateChange_MoveForward":
            case "StateChange_MoveBackward":
              this.walk ();
            break;
            default:
              this.stop ();
            break;
          }
      }
    }, // StateChange_MoveBackward

    start () {

    },

    // update (dt) {},

    //
    _setMix (anim1, anim2) {
        this.spine.setMix(anim1, anim2, this.mixTime);
        this.spine.setMix(anim2, anim1, this.mixTime);
    },

    // ANIMATIONS
    stop () {
        this.spine.clearTrack(0);
        this._hasStop = true;
    },

    walk () {
      var animationName = 'walk';
      if(undefined === this.state || undefined === this.state[animationName] || 0 === this.state[animationName]) {
        this.spine.setAnimation(0, animationName, true);
        this._hasStop = false;
      }
    },

    run () {
        this.spine.setAnimation(0, 'run', true);
        this._hasStop = false;
    },

    jump () {
      var animationName = 'jump';
      if(undefined === this.state || undefined === this.state[animationName] || 0 === this.state[animationName]) {
        var oldAnim = this.spine.animation;
        console.log(oldAnim);
        this.spine.setAnimation(0, animationName, false);
        cc.log("jump: " + oldAnim);
        if (oldAnim && !this._hasStop) {
            this.spine.addAnimation(0, oldAnim === 'run' ? 'run' : 'walk', true, 0);
        }
      }
    },

    shoot () {
      var animationName = 'shoot';
      if(undefined === this.state || undefined === this.state[animationName] || 0 === this.state[animationName]) {
        this.spine.setAnimation(1, animationName, false);
        this._hasStop = false;
      }
    },


    // OPTIONS
    toggleDebugSlots () {
        this.spine.debugSlots = !this.spine.debugSlots;
    },

    toggleDebugBones () {
        this.spine.debugBones = !this.spine.debugBones;
    },

    toggleTimeScale () {
        if (this.spine.timeScale === 1.0) {
            this.spine.timeScale = 0.3;
        }
        else {
            this.spine.timeScale = 1.0;
        }
    },
});
