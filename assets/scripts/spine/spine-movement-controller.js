/*
 * This class handles the movement for side scrolling games, where:
 * - moving the UP could refer to jump or move upwards to another 'lane'
 * - moving the DOWN could refer to crouch, jump down or move downwards to another 'lane'
 * - left and right would be forward and backward
 *
 * Currently:
 * Only handle forward and backward, not yet move sideways yet.
 */
cc.Class({
    extends: cc.Component,

    properties: {
        // unit moment speed
        move_speed: 10,
        // forward direction
        forward_vector: {
          default: cc.Vec2.UP,
        },
    }, // properties

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start: function() {
      // initialize the private properties //
      this.movement_direction = 0;

      this.resetInputControlFlags();
    }, // start

    // called every frame
    update: function (dt) {
      //cc.log("[spine-movement-controller] update: " + dt);
      // cc.log("[spine-movement-controller] update this.input_left: " + this.input_left);
      // cc.log("[spine-movement-controller] update this.input_right: " + this.input_right);
      // cc.log("[spine-movement-controller] update this.input_up: " + this.input_up);
      // cc.log("[spine-movement-controller] update this.input_down: " + this.input_down);

      // test physics movement
      //this.physicsBody.applyLinearImpulse();

      this.updateMovementManual(dt);
    }, // update

    // input //
    onInputUp: function(flag) {

    }, // onInputUp
    onInputDown: function(flag) {

    }, // onInputDown
    onInputLeft: function(flag) {
      this.mMoveBackward = flag;

      // notify the listeners to this event
      if(this.mMoveBackward) {
        this.node.emit("state_moved_backward", this);
      } // idle is set during update
    }, // onInputLeft
    onInputRight: function(flag) {
      this.mMoveForward = flag;

      // notify the listeners to this event
      if(this.mMoveForward) {
        this.node.emit("state_moved_forward", this);
      } // idle is set during update
    }, // onInputRight

    // controller logic //
    calculateMovementVector: function() {
      // determine the movement direction
      if ( this.mMoveForward && this.mMoveBackward )
        this.movement_direction = 0;
      else if ( this.mMoveForward )
        this.movement_direction = 1;
      else if( this.mMoveBackward )
        this.movement_direction = -1;
      else
        this.movement_direction = 0;

      // if there is no command to move dont need to move
      if(0 != this.movement_direction) {
        // factor the user input control
        return this.forward_vector.mul(this.movement_direction);
      }
      return cc.Vec2.ZERO;
    }, // calculateMovementVector

    updateMovementManual: function(dt) {
      ////////////////////////////////////////////////////////////////////////////
      // determine the movement direction
      var movementDirection = this.calculateMovementVector();

      // move this node
      if( 0 != this.move_speed )
      {
        if (0 === movementDirection.x && 0 === movementDirection.y) {
          console.log("movementDirection: ");
          console.log(movementDirection);
          this.node.emit("state_idle", this);
        } else {
          if( 0 != movementDirection.x )
            this.node.x += movementDirection.x * this.move_speed * dt;
          if( 0 != movementDirection.y )
            this.node.y += movementDirection.y * this.move_speed * dt;
        }
      }
      ////////////////////////////////////////////////////////////////////////////
    }, // updateMovementManual

    resetInputControlFlags: function() {
      // input control flags
      this.input_left = false;
      this.input_right = false;
      this.input_up = false;
      this.input_down = false;
    }, // ResetInputControlFlags
});
