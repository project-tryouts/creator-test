/*
 * Character needs to control state.
 *
 * TODO:
 * - implement 'grounded' check, to determine if the character is on the ground.
 * - implement the different states and state transistions.
 * - each state handles the modular scripts to interact with their features.
 *
 * reference:
 * - 'HeroControl.js' in the example project of CocosCreator.
 *   - determine aabb collision
 */
cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    onEnable: function () {
      console.log("onEnable");
      cc.director.getCollisionManager().enabled = true;
      cc.director.getCollisionManager().enabledDebugDraw = true;
    },

    onDisable: function () {
      console.log("onDisable");
      cc.director.getCollisionManager().enabled = false;
      cc.director.getCollisionManager().enabledDebugDraw = false;
    },

    // update (dt) {},


    // collision detection
    onCollisionEnter: function (other, self) {
      console.log("onCollisionEnter");
    }, // onCollisionEnter

    onCollisionStay: function (other, self) {
      console.log("onCollisionStay");
    }, // onCollisionStay

    onCollisionExit: function (other, self) {
      console.log("onCollisionExit");
    }, // onCollisionExit
});
