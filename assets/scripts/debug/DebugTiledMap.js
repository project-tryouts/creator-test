// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

        tiled_map : {
          default: null,
          type: cc.TiledMap,
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
      var canvas = cc.find('Canvas');
      canvas.on(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this);
    }, // onLoad

    start: function () {

    }, // start

    // update: function (dt) {}, // update

    // [Touch] input handler
    onTouchBegan: function (event) {
        var scene = cc.director.getScene();
        var touchLoc = event.touch.getLocation();
        // cc.log("====================================");
        // cc.log("[DebugTiledMap] onTouchBegan: {" + touchLoc.x + ", " + touchLoc.y + "}");

        if (null !== this.tiled_map) {
          // convert the touch position to tiled map node position
          var nodePos = this.tiled_map.node.convertToNodeSpace(touchLoc);
          //cc.log(`nodePos pos: {${nodePos.x}, ${nodePos.y}}`);

          // calculate the tile position
          var tilePos = new cc.Vec2(Math.trunc(nodePos.x / this.tiled_map.getTileSize().width),
                                    Math.trunc(nodePos.y / this.tiled_map.getTileSize().height));
          // reverse the y-axis
          tilePos.y = Math.abs(tilePos.y - this.tiled_map.getMapSize().height) - 1;

          // cc.log(`tilePos pos: {${tilePos.x}, ${tilePos.y}}`);
          // cc.log(`this.tiled_map.node size: {${this.tiled_map.node.width}, ${this.tiled_map.node.height}}`);
          // cc.log(`this.tiled_map.node pos: {${this.tiled_map.node.y}, ${this.tiled_map.node.x}}`);
          // cc.log(`this.tiled_map map size: {${this.tiled_map.getMapSize().width}, ${this.tiled_map.getMapSize().height}}`);
          //
          //
          // cc.log(`0 <= tilePos.x: {${0 <= tilePos.x}}`);
          // cc.log(`0 <= tilePos.y: {${0 <= tilePos.y}}`);
          // cc.log(`tilePos.x < this.tiled_map.getMapSize().width: {${tilePos.x < this.tiled_map.getMapSize().width}}`);
          // cc.log(`tilePos.y < this.tiled_map.getMapSize().height: {${tilePos.y < this.tiled_map.getMapSize().height}}`);

          // // check if the touch tile position is within bounds
          // if(0 <= tilePos.x && tilePos.x < this.tiled_map.getMapSize().width &&
          //    0 <= tilePos.y && tilePos.y < this.tiled_map.getMapSize().height)
          // {
          //   var layers = this.tiled_map.allLayers();
          //     for (var i = 0; i < layers.length; ++i) {
          //       var layer = layers[i];
          //       var gUid = layer.getTileGIDAt(tilePos);
          //       cc.log(`Layers[${i}][${layer.name}]: ${gUid}`);
          //     }
          //   }
          // } else {
          //   // does not seems to print these logs
          //   cc.log("out of bounds");
          //   console.info("out of bounds");
          // }
      } // null !== this.tiled_map
    }, // onTouchBegan
});
