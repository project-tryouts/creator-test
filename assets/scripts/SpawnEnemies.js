// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

        // spawn intervals


        // list of position to spawn
        spawnpoints: {
          default: [],
          type: cc.Node
        },

        // list of enemies to spawn
        enemies: {
          default: [],
          type: cc.Prefab
        },

        // timer in between each spawn
        cooldownTimer: 3,
    },

    //
    spawnObject: function (prefab, worldPos) {
      cc.assert(prefab, "Null [prefab] parameter");
      cc.assert(worldPos, "Null [position] parameter");

      // create the prefab instance
      var prefabInstance = cc.instantiate(prefab);
      // add to the this node
      this.node.addChild(prefabInstance);
      // set the new instance position
      var nodePos = this.node.convertToNodeSpaceAR(worldPos);
      prefabInstance.position = nodePos;

      cc.log("[SpawnEnemies] spawnObject position: {" + prefabInstance.position.x + ", " + prefabInstance.position.y + "}");
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
      // this will reference the variable from the script,
      // which repesent a global variable.
      //this.timer = require("Timer");
      this.timer = Object.create(require("Timer"));
      this.timer.timerLimit = this.cooldownTimer;

      // ready the cooldown for the first time
      this.timer.update(this.timer.timerLimit);
    }, // onLoad function

    start: function () {

    }, // start function

    update: function (dt) {
      this.timer.update(dt);

      if( this.timer.IsReady() ) {
        // reset the timer
        this.timer.reset();

        // reference:
        // https://stackoverflow.com/questions/4550505/getting-a-random-value-from-a-javascript-array
        //var rand = myArray[Math.floor(Math.random() * myArray.length)];
        // randomly get a prefab from the array
        var randomEnemyPrefab = this.enemies[Math.floor(Math.random() * this.enemies.length)];
        // randomly get a node from the array
        var randomSpawnNode = this.spawnpoints[Math.floor(Math.random() * this.spawnpoints.length)];
        if (null !== randomEnemyPrefab && null !== randomSpawnNode) {
          var parentNode = randomSpawnNode.parent;
      		var worldPos = parentNode.convertToWorldSpaceAR(randomSpawnNode.position);
          this.spawnObject(randomEnemyPrefab, worldPos);
        }
        else
        {
          cc.log("[SpawnEnemies] update: null randomEnemyPrefab OR  randomSpawnNode");
        }
      }

    }, // update function


});
