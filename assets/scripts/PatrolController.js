// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

        // loop back to the first point, upon reaching the end waypoint
        loop: false,
        // speed per pixel
        speed: 1000,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
      // the path of positions for the patrol path, relative to the world position
      this.routes = [];
      // set the current index
      this.routeIndex = 0;

      // if(0 == this.routes.length)
      //   this.generateRandomPosition ();
      this.processA ();
      //this.begin ();
    }, // start

    generateRandomPosition () {
      cc.log("[patrolController] generateRandomPosition");

      // get the window size
      var visibleOrigin = cc.director.getVisibleOrigin();
      var visibleSize = cc.director.getVisibleSize();

      var visibleRect = new cc.Rect(visibleOrigin.x, visibleOrigin.y, visibleSize.width, visibleSize.height);
      visibleRect.x += this.node.width;
      visibleRect.width -= this.node.width;
      visibleRect.y += this.node.height;
      visibleRect.height -= this.node.height;


      // generate a random position
      var pos = new cc.Vec2(cc.Vec2.ZERO);
      pos.x = visibleRect.xMax * cc.random0To1() - visibleRect.xMin;
      pos.y = visibleRect.yMax * cc.random0To1() - visibleRect.yMin;
      cc.log("[patrolController] generateRandomPosition pos: {" + pos.x + ", " + pos.y + "}");

      var localPos = this.node.convertToNodeSpace(pos);
      this.routes.push(localPos);

      cc.log("[patrolController] generateRandomPosition this.routes.length: " + this.routes.length);
    }, // generateRandomPosition

    processA () {
      //cc.log("[patrolController] processA");

      //cc.log("[patrolController] processA this.routes.length: " + this.routes.length);

      if(0 < this.routes.length) {
        //cc.log("[patrolController] processA: begin next cycle ");
        this.begin ();
      }
    }, // processA

    // update (dt) { },

    reached () {
      //cc.log("[patrolController] reached: [" + this.routeIndex + "/" + this.routes.length + "]");

      // increase the current count
      this.routeIndex++;

      // check if the index is at the max and there is a flag to loop back
      if(this.loop && this.routeIndex === this.routes.length) {
        this.routeIndex = 0;
        //cc.log("[patrolController] processA: reseting to start");
      }

      // if(0 == this.routes.length)
      //   this.generateRandomPosition ();
      this.processA ();
    }, // reached

    begin () {
      // check if the current index is at the max
      if(this.routeIndex !== this.routes.length) {
        // get the target position
        //var targetWorldPos = this.routes.shift();
        var targetWorldPos = this.routes[this.routeIndex];
        var parentNode = this.node.parent;
        var targetPos = parentNode.convertToNodeSpaceAR(targetWorldPos);

        // cc.log("[patrolController] begin targetWorldPos: {" + targetWorldPos.x + ", " + targetWorldPos.y + "}");
        // cc.log("[patrolController] begin targetPos: {" + targetPos.x + ", " + targetPos.y + "}");
        // cc.log("[patrolController] begin this.node.position: {" + this.node.position.x + ", " + this.node.position.y + "}");

        // get the vector difference
        var differenceVector = targetPos.sub(this.node.position);
        //cc.log("[patrolController] begin differenceVector: {" + differenceVector.x + ", " + differenceVector.y + "}");
        var distance = differenceVector.mag();
        //cc.log("[patrolController] begin distance: " + distance);
        var timeRequired = distance / this.speed;
        //cc.log("[patrolController] begin timeRequired: " + timeRequired);

        var time = timeRequired;
        var moveTo = cc.moveTo(time, targetPos);

        // CallFunc without data
        var finish = cc.callFunc(this.reached, this);

        // combine all the actions to a sequence
        var actionSeq = cc.sequence(moveTo, finish);
        this.node.runAction(actionSeq);
      }
    }, // begin
});
