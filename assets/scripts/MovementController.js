// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

        // flag to move the object using physics
        use_physics: false,
        // unit moment speed
        move_speed: 10,
        // unit rotation speed
        rotation_speed: 10,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start: function() {
      // initialize the private properties //
      this.rotation_direction = 0;
      this.movement_direction = 0;

      // the facing direction will be calculated based on the rotation of the node
      this.facing_direction = cc.Vec2.ZERO;

      this.resetInputControlFlags();

      // check if the physics flag is enabled and check for the physics body component
      if (this.use_physics) {
        var physicsBody = this.getComponent(cc.RigidBody);
        cc.assert(undefined === physicsBody || null === physicsBody, "No physics component in this node");
        this.physicsBody = physicsBody;
      }
    }, // start

    // called every frame
    update: function (dt) {
      //cc.log("[MovementController] update: " + dt);
      // cc.log("[MovementController] update this.input_rotation_left: " + this.input_rotation_left);
      // cc.log("[MovementController] update this.input_rotation_right: " + this.input_rotation_right);
      // cc.log("[MovementController] update this.input_move_forward: " + this.input_move_forward);
      // cc.log("[MovementController] update this.input_move_backward: " + this.input_move_backward);

      // test physics movement
      //this.physicsBody.applyLinearImpulse();

      this.updateMovementManual(dt);
    }, // updateupdate

    calculateMovementVector: function() {
      // determine the momvent direction
      if ( this.input_move_forward && this.input_move_backward )
        this.movement_direction = 0;
      else if ( this.input_move_forward )
        this.movement_direction = 1;
      else if( this.input_move_backward )
        this.movement_direction = -1;
      else
        this.movement_direction = 0;

      // if there is no command to move dont need to move
      if(0 != this.movement_direction) {
        // calculate the forward vector
        this.determineForwardVector();

        // factor the user input control
        return this.facing_direction.mul(this.movement_direction);
      }
      return cc.Vec2.ZERO;
    },

    determineForwardVector: function() {
      // calculate the facing direction based on the node's rotation
      this.facing_direction = cc.Vec2.UP;
      var degreeRadian = cc.degreesToRadians(this.node.rotation) * -1;
      this.facing_direction = this.facing_direction.rotate(degreeRadian);
      // cc.log("[MovementController] update rotation: {" + this.node.rotation + "}");
      // cc.log("[MovementController] update degreeRadian: {" + degreeRadian + "}");
      // cc.log("[MovementController] update facing firection: {" + this.facing_direction.x + ", " + this.facing_direction.y + "}");
    },

    updateMovementManual: function(dt) {
      ////////////////////////////////////////////////////////////////////////////
      // rotation //
      // determine the rotation direction
      if( this.input_rotation_left && this.input_rotation_right )
        this.rotation_direction = 0;
      else if ( this.input_rotation_left )
        this.rotation_direction = -1;
      else if( this.input_rotation_right )
        this.rotation_direction = 1;
      else
        this.rotation_direction = 0;

      // rotate the node
      if( 0 != this.rotation_speed && 0 != this.rotation_direction )
        this.node.rotation += this.rotation_direction * this.rotation_speed * dt;
      ////////////////////////////////////////////////////////////////////////////
      // determine the momvent direction
      var movementDirection = this.calculateMovementVector();

      // move this node
      if( 0 != this.move_speed )
      {
        if( 0 != movementDirection.x )
          this.node.x += movementDirection.x * this.move_speed * dt;
        if( 0 != movementDirection.y )
          this.node.y += movementDirection.y * this.move_speed * dt;
      }
      ////////////////////////////////////////////////////////////////////////////
    },

    resetInputControlFlags: function() {
      // input control flags
      this.input_rotation_left = false;
      this.input_rotation_right = false;
      this.input_move_forward = false;
      this.input_move_backward = false;
    }, // ResetInputControlFlags
});
