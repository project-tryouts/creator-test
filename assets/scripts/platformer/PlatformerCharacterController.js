// addtional reference to add scripts to CocosCreator
// http://docs.cocos.com/creator/manual/en/
// http://www.cocos2d-x.org/docs/api-ref/creator/v1.8/en/
// http://www.cocos2d-x.org/docs/api-ref/creator/v1.9/en/classes/Node.html#dispatchevent
// http://www.cocos2d-x.org/docs/api-ref/creator/v1.9/en/classes/Node.html#on

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
      // set the collision direction
      this.collisionX = 0;
      this.collisionY = 0;
    }, // onLoad

    // start () {},

    // update (dt) {},


    onEnable: function () {
      console.log("onEnable");
      cc.director.getCollisionManager().enabled = true;
      cc.director.getCollisionManager().enabledDebugDraw = true;
    },

    onDisable: function () {
      console.log("onDisable");
      cc.director.getCollisionManager().enabled = false;
      cc.director.getCollisionManager().enabledDebugDraw = false;
    },

    // collision detection
    onCollisionEnter: function (other, self) {
      console.log("onCollisionEnter");
      console.log(self); console.log(other);

      // 1st step
      // get pre aabb, go back before collision
      var otherAabb = other.world.aabb;
      var otherPreAabb = other.world.preAabb.clone();

      var selfAabb = self.world.aabb;
      var selfPreAabb = self.world.preAabb.clone();

      // 2nd step
      // forward x-axis, check whether collision on x-axis
      selfPreAabb.x = selfAabb.x;
      otherPreAabb.x = otherAabb.x;

      // check if hit a collider while moving on the x-axis
      if (cc.Intersection.rectRect(selfPreAabb, otherPreAabb)) {
        // check if the object is collided at the lowest x-axis
          if (selfPreAabb.xMax > otherPreAabb.xMax) {
            // bound this node x position to the lowest value
              this.node.x = otherPreAabb.xMax - this.node.parent.x;
              this.collisionX = -1;


              //this.node.dispatchEvent();

          }
          // check if the object is collided at the highest x-axis
          else if (selfPreAabb.xMin < otherPreAabb.xMin) {
              this.node.x = otherPreAabb.xMin - selfPreAabb.width - this.node.parent.x;
              this.collisionX = 1;
          }

          //this.speed.x = 0;
          other.touchingX = true;
          return;
      }

      // 3rd step
      // forward y-axis, check whether collision on y-axis
      selfPreAabb.y = selfAabb.y;
      otherPreAabb.y = otherAabb.y;

      // check if hit a collider while moving on the y-axis
      if (cc.Intersection.rectRect(selfPreAabb, otherPreAabb)) {
        let spriteOffsetY = this.node.height * 0.5;

        // check if the object is collided at the lowest y-axis
          if (selfPreAabb.yMax > otherPreAabb.yMax) {
            // get the lowest point and add the sprite offset
              this.node.y = otherPreAabb.yMax - this.node.parent.y + spriteOffsetY;
              this.jumping = false;
              this.collisionY = -1;

              console.log("this.node.contentSize: {" , this.node.width, ", " , this.node.height, "}");

              console.log("this.node.y: ", this.node.y);
              console.log("selfPreAabb.yMin: ", selfPreAabb.yMin);
              console.log("otherPreAabb.yMax: ", otherPreAabb.yMax);
              console.log("this.node.parent.y: ", this.node.parent.y);

              this.node.emit("gravity_off", this.collisionX);
          }
          // check if the object is collided at the highest y-axis
          else if (selfPreAabb.yMin < otherPreAabb.yMin) {
            // get the highest point and add the sprite offset
              this.node.y = otherPreAabb.yMin - selfPreAabb.height - this.node.parent.y - spriteOffsetY;
              this.collisionY = 1;
              this.node.emit("gravity_on", this.collisionX);
          }
          else {
            this.node.emit("gravity_on", this.collisionX);
          }

          //this.speed.y = 0;
          other.touchingY = true;
      }
    }, // onCollisionEnter

    onCollisionStay: function (other, self) {
      console.log("onCollisionStay");
    }, // onCollisionStay

    onCollisionExit: function (other, self) {
      console.log("onCollisionExit");
      console.log(self); console.log(other);

      if (other.touchingX) {
          this.collisionX = 0;
          other.touchingX = false;
      }
      else if (other.touchingY) {
          other.touchingY = false;
          this.collisionY = 0;
          this.jumping = true;
      }
    }, // onCollisionExit
});
