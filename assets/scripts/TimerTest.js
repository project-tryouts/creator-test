
function NewTimer(limit) {
	//this = new object();
	this.timerLimit = 0;
	this.timerElapsed = 0;

	this.timerElapsed = this.timerLimit = limit;
	return this;
}

NewTimer.prototype.update = function() {
	this.timerElapsed += dt;
	// ensure the elapsed count is within the limit bounds
	if(this.timerLimit <= this.timerElapsed)
		this.timerElapsed = this.timerLimit;
};

module.exports = NewTimer;
