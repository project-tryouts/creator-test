
cc.Class({
    extends: cc.Component,

    properties: {
      stat_name: {
        default: "",
        tooltip: 'State Name',
      }, // stat_name
      next_state_names: {
        default: [],
        tooltip: 'Next State Names',
      }, // next_state_name
      default_next_state: {
        default: "",
        tooltip: 'Default Next State Name',
      }, // default_next_state

      // IsStateAccessable
      // OnStateEnter
      // OnStateRunning
      // OnStateExit
    }, // properties

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    // update (dt) {},
});
