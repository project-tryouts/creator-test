
cc.Class({
    extends: cc.Component,

    properties: {
      default_start_state: {
        default: "",
        tooltip: 'Default Start State Name',
      }, // default_start_state

      states: {
        default: [],
        tooltip: 'Possible States',
      }, // states
    }, // properties

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    // update (dt) {},
});
