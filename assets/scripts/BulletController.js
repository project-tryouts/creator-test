// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

		facingDirection:{
			default: cc.Vec2.ZERO,
		},
		speed: 100,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.director.getCollisionManager().enabled = true;
        cc.director.getCollisionManager().enabledDebugDraw = true;
	},

    onDisable: function () {
        cc.director.getCollisionManager().enabled = false;
        cc.director.getCollisionManager().enabledDebugDraw = false;
    },

    start () {
		// use the vector{0,1}, because it uses trigo to calculate the angle.
		var origin = cc.Vec2.UP;
		// add a slight offset to the destination
		var offset = this.facingDirection.mul(1);
		var targetPostition = origin.add(offset);
		// calculate the signed angle, so that the angle will be calculated properly based on the 360 degree.
		// if its not signed, it will return a degree between 0 to 180.
		var radianAngle = origin.signAngle (targetPostition);
		var degreeAngle = cc.radiansToDegrees(radianAngle);
		
		// set the node's rotation degree
		this.node.rotation = degreeAngle;
    },

    onCollisionEnter: function (other, self) {
      // destroy myself
      this.node.destroy();
      // destroy the other node
      other.node.destroy();
    }, // onCollisionEnter

    // called every frame, uncomment this function to activate update callback
    update: function (dt) {
		//if(0 != this.facingDirection.x)
			this.node.x += this.speed * this.facingDirection.x * dt;
		//if(0 != this.facingDirection.y)
			this.node.y += this.speed * this.facingDirection.y * dt;
    },
});
