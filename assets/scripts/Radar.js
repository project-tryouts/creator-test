// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

// reference:
// http://www.cocos2d-x.org/docs/creator/manual/en/physics/collision/collision-manager.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

        // increase the size of the collider per frame
        increase_size_speed: 1,
        // the max size of the collider, before it resets
        max_collider_size: 100,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        cc.director.getCollisionManager().enabled = true;
        cc.director.getCollisionManager().enabledDebugDraw = true;
	},

    onDisable: function () {
        cc.director.getCollisionManager().enabled = false;
        cc.director.getCollisionManager().enabledDebugDraw = false;
    },

    start: function () {
      cc.eventManager.addListener({
          event: cc.EventListener.TOUCH_ONE_BY_ONE,
          onTouchBegan: (touch, event) => {
              var touchLoc = touch.getLocation();

              //
              var colliderBox = this.node.getComponent(cc.BoxCollider);
              var colliderPolygon = this.node.getComponent(cc.PolygonCollider);
              var colliderCircle = this.node.getComponent(cc.CircleCollider);
              cc.log("[Radar] start colliderBox: {" + colliderBox + "}");
              cc.log("[Radar] start colliderPolygon: {" + colliderPolygon + "}");
              cc.log("[Radar] start colliderCircle: {" + colliderCircle + "}");

              var colliderPhysicsBox = this.getComponent("PhysicsBoxCollider");
              var colliderPhysicsPolygon = this.getComponent("PhysicsPolygonCollider");
              var colliderPhysicsCircle = this.getComponent("PhysicsCircleCollider ");



              var thisParentNode = this.node.parent;
              var thisWorldPos = thisParentNode.convertToWorldSpaceAR(this.node.position);
              thisWorldPos.addSelf(colliderCircle.offset);
              var thisObjectCircle = {position: thisWorldPos, radius :colliderCircle.radius};
              var otherObjectCircle = {position: touchLoc, radius :1};
              if(cc.Intersection.circleCircle (thisObjectCircle, otherObjectCircle)) {
                cc.log('Hit');
              }
              else {
                cc.log('Not hit');
              }

              cc.log("[Radar] start thisWorldPos: {" + thisWorldPos.x + ", " + thisWorldPos.y + "}");
              cc.log("[Radar] start touchLoc: {" + touchLoc.x + ", " + touchLoc.y + "}");

              // // Get the points in world location of the polygon collider component.
              // // If it's other type colidder component, can also find the test function in cc.Intersection.
              // if (cc.Intersection.pointInPolygon(touchLoc, this.polygonCollider.world.points)) {
              //   cc.log('Hit');
              // }
              // else {
              //   cc.log('Not hit');
              // }

              return true;
          },
      }, this.node);
    }, // start

    update: function (dt) {


      var colliderCircle = this.node.getComponent(cc.CircleCollider);

      colliderCircle.radius +=  this.increase_size_speed * dt;
      if(this.max_collider_size <= colliderCircle.radius) {
        colliderCircle.radius = 1;
      }

      //cc.log(`[Radar] update colliderCircle.radius: ${colliderCircle.radius}`);
    }, // update

    /**
     * Call when a collision is detected
     * @param  {Collider} other The other Collider Component
     * @param  {Collider} self  Self Collider Component
     */
    onCollisionEnter: function (other, self) {
      console.log('on collision enter');

      // Collider Manager will calculate the value in world coordinate system, and put them into the world property
      var world = self.world;

      // Collider Component aabb bounding box
      var aabb = world.aabb;

      // Last calculated Collider Component aabb bounding box
      var preAabb = world.preAabb;

      // world transform
      var t = world.transform;

      // Circle Collider Component world properties
      var r = world.radius;
      var p = world.position;

      // Rect and Polygon Collider Component world properties
      var ps = world.points;

    }, //onCollisionEnter

    /**
     * Call after enter collision, before end collision, and after every time calculate the collision result.
     * @param  {Collider} other The other Collider Component
     * @param  {Collider} self  Self Collider Component
     */
    onCollisionStay: function (other, self) {
      console.log('on collision stay');
    }, // onCollisionStay

    /**
     * Call after end collision
     * @param  {Collider} other The other Collider Component
     * @param  {Collider} self  Self Collider Component
     */
    onCollisionExit: function (other, self) {
        console.log('on collision exit');
    }, // onCollisionExit
});
