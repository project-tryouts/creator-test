// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

// addtional reference to add scripts to CocosCreator
// http://www.cocos2d-x.org/docs/creator/manual/en/scripting/load-assets.html


var GameStatsType = cc.Enum({
    None: 0,
    Health: 1,
    Speed: 2,

    Attack_Min: 11,
    Attack_Max: 12,
    Attack_Speed: 13,
});


cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

        type: {
            default: GameStatsType.None,
            type: GameStatsType,
            tooltip: CC_DEV && 'COMPONENT.GameStatsType',
        },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
		// example to parse json string
		var data = "{\"health\": 100}";
		var someData_notJSON = JSON.parse(data);
		cc.log("[UnitController] someData_notJSON.health: " + someData_notJSON.health);

		// an example to test to load resources
		var jsonPath = "config/stats.json";
		cc.loader.loadRes(jsonPath, function (err, jsonString) {
			cc.log("[UnitController] cc.loader.load jsonString: " + jsonString);
			cc.log("[UnitController] cc.loader.load err: " + err);
		});

		// Remote loading of resources
		var remoteUrl = "http://ip.jsontest.com/";
		cc.loader.load(remoteUrl, function (err, result) {
			cc.log("[UnitController] cc.loader.load result: " + result);
			cc.log("[UnitController] cc.loader.load err: " + err);

			// only useable for url [http://ip.jsontest.com/]
			var jsonTest = JSON.parse(result);
			cc.log("[UnitController] jsonTest.ip: " + jsonTest.ip);
		});
    },

    // update (dt) {},
});
