// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

// reference:
// http://docs.cocos.com/creator/manual/en/scripting/reference/class.html#type

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

		unit_controller: {
			default: null,
			type: cc.Component,
		},

    movement_controller: {
			default: null,
      //type: cc.Component,
			type: require('./MovementController'),
		},

		touchPosition: cc.Vec2.ZERO,
    },

	// button pressed state
	directionLeft:false,
	directionRight:false,
	directionUp:false,
	directionDown:false,


  touchAction:false,
	actionA:false,

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
		//add keyboard input listener to call turnLeft and turnRight
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
		cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

        var canvas = cc.find('Canvas');
        canvas.on(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this);
		canvas.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMoved, this);
		canvas.on(cc.Node.EventType.TOUCH_END, this.onTouchEnded, this);
		canvas.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCanceled, this);



    var visibleSize = cc.director.getVisibleSize();
    var visibleOrigin = cc.director.getVisibleOrigin();


    // var targetNode = this.movement_controller.node;
    // // creates the action with a set boundary
    // var followAction = cc.follow(targetNode, cc.rect(visibleOrigin.x, visibleOrigin.y, visibleSize.width, visibleSize.height));
    // //this.node.runAction(followAction);
    // Camera.main.node.runAction(followAction);


		/*
		canvas.on(cc.Node.EventType.MOUSE_DOWN, this.onTouchBegan, this);
		canvas.on(cc.Node.EventType.MOUSE_MOVE, this.onTouchBegan, this);
		canvas.on(cc.Node.EventType.MOUSE_ENTER, this.onTouchBegan, this);
		canvas.on(cc.Node.EventType.MOUSE_LEAVE, this.onTouchBegan, this);
		canvas.on(cc.Node.EventType.MOUSE_UP, this.onTouchBegan, this);
		canvas.on(cc.Node.EventType.MOUSE_WHEEL, this.onTouchBegan, this);
		*/
	},

	onDestroy: function () {
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
		cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    },

    start () {
      // unit controller script is no longer a MUST, this is for testing purpose
		//cc.assert(this.unit_controller, "Invalid unit controller");
    },

	// [Touch] input handler
    onTouchBegan: function (event) {
        var scene = cc.director.getScene();
        var touchLoc = event.touch.getLocation();

        this.touchAction = true;
        this.touchPosition = touchLoc;
        //cc.log("[PlayerController] onTouchBegan: {" + this.touchPosition.x + ", " + this.touchPosition.y + "}");

		/*
        var bullet = cc.instantiate(this.bullet);
        bullet.position = touchLoc;
        bullet.active = true;
        scene.addChild(bullet);
		*/

    }, // onTouchBegan

    onTouchMoved: function (event) {
        var scene = cc.director.getScene();
        var touchLoc = event.touch.getLocation();


		this.touchPosition = touchLoc;
		//cc.log("[PlayerController] onTouchMoved: {" + this.touchPosition.x + ", " + this.touchPosition.y + "}");
    }, // onTouchMoved

    onTouchEnded: function (event) {
        var scene = cc.director.getScene();
        var touchLoc = event.touch.getLocation();

        this.touchPosition = touchLoc;
        //cc.log("[PlayerController] onTouchEnded: {" + this.touchPosition.x + ", " + this.touchPosition.y + "}");

        // reset the touch parameters
        this.touchPosition = cc.Vec2.ZERO;
        this.touchAction = false;
    }, // onTouchEnded

    onTouchCanceled: function (event) {
        var scene = cc.director.getScene();
        var touchLoc = event.touch.getLocation();

        this.touchPosition = touchLoc;
        //cc.log("[PlayerController] onTouchCanceled: {" + this.touchPosition.x + ", " + this.touchPosition.y + "}");

        // reset the touch parameters
        this.touchPosition = cc.Vec2.ZERO;
        this.touchAction = false;
    }, // onTouchCanceled

	// input handler
	onKeyDown (event) {
        switch(event.keyCode) {
            case cc.KEY.a:
            case cc.KEY.left:
                //cc.log('left direction button pressed');
                this.directionLeft = true;
                break;
            case cc.KEY.d:
            case cc.KEY.right:
                //cc.log('right direction button pressed');
                this.directionRight = true;
                break;

            case cc.KEY.w:
            case cc.KEY.up:
                //cc.log('up direction button pressed');
                this.directionUp = true;
                break;
            case cc.KEY.s:
            case cc.KEY.down:
                //cc.log('down direction button pressed');
                this.directionDown = true;
                break;

			case cc.KEY.space:
				//cc.log('space button pressed');
				this.actionA = true;
				break;
        }
    }, // onKeyDown

	onKeyUp (event) {
        switch(event.keyCode) {
            case cc.KEY.a:
            case cc.KEY.left:
                //cc.log('left direction button released');
                this.directionLeft = false;
                break;
            case cc.KEY.d:
            case cc.KEY.right:
                //cc.log('right direction button released');
                this.directionRight = false;
                break;

            case cc.KEY.w:
            case cc.KEY.up:
                //cc.log('up direction button released');
                this.directionUp = false;
                break;
            case cc.KEY.s:
            case cc.KEY.down:
                //cc.log('down direction button released');
                this.directionDown = false;
                break;

			case cc.KEY.space:
				//cc.log('space button released');
				this.actionA = false;
				break;
        }


    }, // onKeyUp

    update (dt) {
      if(null !== this.movement_controller) {
            this.movement_controller.input_move_forward = this.directionUp;
            this.movement_controller.input_move_backward = this.directionDown;

            this.movement_controller.input_rotation_left = this.directionLeft;
            this.movement_controller.input_rotation_right = this.directionRight;

            // var typeCheck = (typeof this.movement_controller === 'MovementController');
            // cc.log("[PlayerController] onKeyUp typeCheck: " + typeCheck);
            // cc.log("[PlayerController] onKeyUp object type: " + typeof this.movement_controller);
            // cc.log("[PlayerController] onKeyUp this.movement_controller.input_rotation_left: " + this.movement_controller.input_rotation_left);
            // cc.log("[PlayerController] onKeyUp this.movement_controller.input_rotation_right: " + this.movement_controller.input_rotation_right);
            // cc.log("[PlayerController] onKeyUp this.movement_controller.input_move_forward: " + this.movement_controller.input_move_forward);
            // cc.log("[PlayerController] onKeyUp this.movement_controller.input_move_backward: " + this.movement_controller.input_move_backward);
      }
    }, // update
});
