// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

// addtional reference to add scripts to CocosCreator
// http://docs.cocos.com/creator/manual/en/
// http://www.cocos2d-x.org/docs/api-ref/creator/v1.8/en/
//
// https://code.tutsplus.com/tutorials/stop-nesting-functions-but-not-all-of-them--net-22315
// http://bonsaiden.github.io/JavaScript-Garden/
// http://javascript.ruanyifeng.com/oop/pattern.html

var timer = {
	timerElapsed: 0,
	timerLimit: 0,

	// returns the percentage between 0 - 1
    GetPercentage: function() {
		var percentage = 0;

		if(0 != this.timerLimit)
			return this.timerElapsed / this.timerLimit;
		return this.percentage;
	},

	// check if the timer have reached the limit
	IsReady: function() {
		if(this.timerLimit <= this.timerElapsed)
			return true;
		return false;
	},

	// add the delta time to the timer
	update: function (dt) {
		this.timerElapsed += dt;
		// ensure the elapsed count is within the limit bounds
		if(this.timerLimit <= this.timerElapsed)
			this.timerElapsed = this.timerLimit;
	},

	// reset the elapsed timer
	reset: function () {
		this.timerElapsed = 0;
	},
};

module.exports = timer;
