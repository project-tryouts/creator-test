// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
      // line color
      line_color: new cc.Color(255, 255, 255, 255),
      line_width: 1,

      // fill color
      blocked_fill_color: new cc.Color(255, 255, 255, 255),

      label_prefab: {
        default: null,
        type: cc.Prefab
      },

      tile_size: {
        default: cc.Size.ZERO,
      }, // tile_size

      map_size: {
        default: cc.Size.ZERO,
      }, // map_size
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
      // create a [graphic] component object
      this.graphics = this.node.addComponent(cc.Graphics);
    }, // onLoad

    start: function () {
      // var mapSize = new cc.Size(8, 4);
      // var tileSize = new cc.Size(32, 32);
      var mapSize = this.map_size;
      var tileSize = this.tile_size;
      // cc.log(`[DebugDrawGrid] update mapSize: {${mapSize.width}, ${mapSize.height}}`);
      // cc.log(`[DebugDrawGrid] update tileSize: {${mapSize.width}, ${mapSize.height}}`);

      //  set the render color
      this.graphics.fillColor = this.blocked_fill_color;
      this.graphics.strokeColor = this.line_color;
      this.graphics.lineWidth = this.line_width;

      var startPos = new cc.Vec2(0, 0);
      // loop the columns then loop the rows
      for (var colCount = 0; colCount < mapSize.height + 1; colCount++) {
        var posStart = new cc.Vec2(startPos.x, colCount * tileSize.height);
        var posEnd = new cc.Vec2(mapSize.width * tileSize.width, colCount * tileSize.height);

        // cc.log(`[DebugDrawGrid] update posStart: {${posStart.x}, ${posStart.y}}`);
        // cc.log(`[DebugDrawGrid] update posEnd: {${posEnd.x}, ${posEnd.y}}`);

        this.graphics.moveTo(posStart.x, posStart.y);
        this.graphics.lineTo(posEnd.x, posEnd.y);
        this.graphics.stroke();
      } // loop map height

      for (var rowCount = 0; rowCount < mapSize.width + 1; rowCount++) {
        var posStart = new cc.Vec2(rowCount * tileSize.width, startPos.y);
        var posEnd = new cc.Vec2(rowCount * tileSize.width, mapSize.height * tileSize.height);

        // cc.log(`[DebugDrawGrid] update posStart: {${posStart.x}, ${posStart.y}}`);
        // cc.log(`[DebugDrawGrid] update posEnd: {${posEnd.x}, ${posEnd.y}}`);

        this.graphics.moveTo(posStart.x, posStart.y);
        this.graphics.lineTo(posEnd.x, posEnd.y);
        this.graphics.stroke();
      } // loop map width


      ////////////////////////////////////////////////////////////////////////////
      for (var i = 0; i < this.blockedTilePositions.length; i++) {
        var gridPos = this.blockedTilePositions[i];


        var gridPixelPos = new cc.Vec2(gridPos.x * tileSize.width, gridPos.y * tileSize.height);
        // reverse the y-axis
        gridPixelPos.y = Math.abs(gridPixelPos.y - (mapSize.height * tileSize.height)) - tileSize.height;
        // color the tiles with solid rectangle, it will not be transparent
        //this.graphics.fillRect(gridPixelPos.x, gridPixelPos.y, tileSize.width, tileSize.height);

        var newNode = cc.instantiate(this.label_prefab);
        var label = newNode.getComponent(cc.Label);
        label.string = `{${gridPos.x},${gridPos.y}}`;
        label.fontSize = 8;
        newNode.x = gridPixelPos.x + tileSize.width * 0.5;
        newNode.y = gridPixelPos.y + tileSize.height * 0.5;
        this.node.addChild(newNode);
        newNode.setLocalZOrder(100);
      }

      // ////////////////////////////////////////////////////////////////////////////
      // var gridPos = new cc.Vec2(1, 3);
      // var gridPixelPos = new cc.Vec2(gridPos.x * tileSize.width, gridPos.y * tileSize.height);
      // this.graphics.fillColor = this.blocked_fill_color;
      // this.graphics.fillRect(gridPixelPos.x, gridPixelPos.y, tileSize.width, tileSize.height);
      ////////////////////////////////////////////////////////////////////////////


//       var newNode = cc.instantiate(this.label_prefab);
//       var label = newNode.getComponent(cc.Label);
//       label.string = "suppp";
//       newNode.x = 100; newNode.y = 100;
// this.node.addChild(newNode);
// newNode.setLocalZOrder(100);
//
//       // get the active current scene
//       var currentScene = cc.director.getScene();
//       cc.assert(currentScene, "No scene detected by [cc.director]");
//       //currentScene.addChild(newNode);
//
//       this.node.addChild(newNode);
//
//       newNode.setLocalZOrder(100);


      // console.log("label");
      // console.log(label);
      // console.log("newNode");
      // console.log(newNode);
    }, // start

    update: function (dt) {

    }, // update
});
