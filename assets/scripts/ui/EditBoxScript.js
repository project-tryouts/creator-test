// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    // update (dt) {},

    editBoxBegin (eventData) {
      // cc.log("[EditBoxScript] editBoxBegin eventData: " + eventData);
      // cc.log("[EditBoxScript] editBoxBegin eventData.string: " + eventData.string);
      // cc.log("[EditBoxScript] editBoxBegin eventData.node: " + eventData.node);
      // cc.log("[EditBoxScript] editBoxBegin eventData.node.name: " + eventData.node.name);
      //
      // // print all the keys in the object
      // console.log(Object.keys(eventData));
      // console.log(Object.keys(eventData.node));
    },

    editBoxUpdate (eventData) {
      cc.log("[EditBoxScript] editBoxUpdate eventData: " + eventData);

    },

    editBoxEnd (eventData) {
      alert("editBoxEnd: " + eventData.string);

      // cc.log("[EditBoxScript] editBoxEnd eventData: " + eventData);
      // cc.log("[EditBoxScript] editBoxEnd eventData.string: " + eventData.string);
      // cc.log("[EditBoxScript] editBoxEnd eventData.node: " + eventData.node);
      // cc.log("[EditBoxScript] editBoxEnd eventData.node.name: " + eventData.node.name);
      //
      // // print all the keys in the object
      // console.log(Object.keys(eventData));
      // console.log(Object.keys(eventData.node));
    },

    editBoxReturn (eventData) {
      alert("editBoxReturn: " + eventData.string);
      
      // cc.log("[EditBoxScript] editBoxReturn eventData: " + eventData);
      // cc.log("[EditBoxScript] editBoxReturn eventData.string: " + eventData.string);
      // cc.log("[EditBoxScript] editBoxReturn eventData.node: " + eventData.node);
      // cc.log("[EditBoxScript] editBoxReturn eventData.node.name: " + eventData.node.name);
      //
      // // print all the keys in the object
      // console.log(Object.keys(eventData));
      // console.log(Object.keys(eventData.node));
    },
});
