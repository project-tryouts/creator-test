cc.Class({
    extends: cc.Component,

    properties: {
      // line color
      line_color: new cc.Color(255, 255, 255, 255),
      line_width: 1,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
      // create a [graphic] component object
      this.graphics = this.node.addComponent(cc.Graphics);
    }, // onLoad

    start () {
      //  set the render color
      this.graphics.strokeColor = this.line_color;
      this.graphics.lineWidth = this.line_width;

      // console.info("[DebugDrawPath] start");
      // console.info(this.path);
      // console.info(this.offset);


      this.redraw();
    }, // start

    // update (dt) {},

    redraw: function () {
      cc.assert (null != this.graphics && undefined != this.graphics, "Null graphic component");

      // clear the previous draw cache
      this.graphics.clear();

      if(null !== this.path && undefined !== this.path && Array.isArray(this.path)) {
        var currentPosition = null;
        for (let i = 0; i < this.path.length; i++) {
          var pos = this.path[i];
          if(null === currentPosition) {
            // set the first point as the current position
            currentPosition = pos;

            // draw a green circle as the first position
            this.graphics.circle(currentPosition.x + this.offset.x, currentPosition.y + this.offset.y, 10);
            this.graphics.fillColor = cc.Color.GREEN;
            this.graphics.fill();
            continue;
          }

          // draw
          this.graphics.moveTo(currentPosition.x + this.offset.x, currentPosition.y + this.offset.y);
          this.graphics.lineTo(pos.x + this.offset.x, pos.y + this.offset.y);
          this.graphics.stroke();

          // update the current position
          currentPosition = pos;
        }
        // draw a red circle as the last position
        this.graphics.circle(currentPosition.x + this.offset.x, currentPosition.y + this.offset.y, 10);
        this.graphics.fillColor = cc.Color.RED;
        this.graphics.fill();
      }
    }, // redraw
});
