// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

// reference:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
// https://stackoverflow.com/questions/3390396/how-to-check-for-undefined-in-javascript
// https://github.com/cocos-creator/engine/issues/791

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

        status_label: {
          default: null,
          type: cc.Label,
        },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    // update (dt) {},


    button_pressed (eventData) {
      cc.log("[ButtonSelection] button_pressed eventData: " + eventData);
      cc.log("[ButtonSelection] button_pressed eventData.type: " + eventData.type);
      cc.log("[ButtonSelection] button_pressed eventData.eventPhase: " + eventData.eventPhase);
      cc.log("[ButtonSelection] button_pressed eventData._eventCode: " + eventData._eventCode);

      cc.log("[ButtonSelection] button_pressed eventData.target: " + eventData.target);
      cc.log("[ButtonSelection] button_pressed eventData.target.name: " + eventData.target.name);

      cc.log("[ButtonSelection] button_pressed eventData.currentTarget: " + eventData.currentTarget);
      cc.log("[ButtonSelection] button_pressed eventData.currentTarget.name: " + eventData.currentTarget.name);

      // print all the keys in the object
      console.log(Object.keys(eventData));
      console.log(Object.keys(eventData.target));
      console.log(Object.keys(eventData.currentTarget));


      if(null !== this.status_label) {
          this.status_label.string = "[ButtonSelection] button_pressed";
      }
    },
});
