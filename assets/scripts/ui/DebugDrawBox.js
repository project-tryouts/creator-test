// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

// reference:
// http://www.cocos2d-x.org/docs/api-ref/creator/v1.8/en/classes/Graphics.html#beziercurveto

cc.Class({
    extends: cc.Component,

    properties: {
        // p/s: 'this' node cannot have the canvas component
        target_node: {
          default: null,
          type: cc.Node,
          tooltip: 'target to draw the AABB, if null use \'this\' node',
        }, // target_node

        // fill status
        fill: false,
        // fill color
        fill_color: new cc.Color(255, 255, 255, 255),

        // fill border status
        fill_border: false,
        // fill border color
        fill_border_color: new cc.Color(255, 255, 255, 255),
        // fill border size
        fill_border_size: 1,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
      // set this node as target, if none is set
      if(null === this.target_node || undefined === this.target_node)
        this.target_node = this.node;

      // create a [graphic] component object
      this.graphics = this.node.addComponent(cc.Graphics);
    }, // onLoad

    start () {
      this.redraw();
    }, // start

    update (dt) {

    }, // update

    redraw: function () {
      cc.assert (null != this.graphics && undefined != this.graphics, "Null graphic component");
      // get the axis align bounding box
      var aabb = this.target_node.getBoundingBoxToWorld();
      console.log("[DebugDrawBox] redraw aabb: ");
      console.log(aabb);

      if(true === this.fill) {
        this.graphics.clear();
        this.graphics.fillColor = this.fill_color;
        this.graphics.fillRect(0, 0, this.target_node.width, this.target_node.height);
        console.log("[DebugDrawBox] redraw: fill graphic");
      }

      if(true === this.fill_border) {
        this.graphics.strokeColor = this.fill_border_color;
        this.graphics.lineWidth = this.fill_border_size;

        this.graphics.rect(aabb.origin.x, aabb.origin.y, aabb.size.width, aabb.size.height);
        this.graphics.stroke();

        console.log("[DebugDrawBox] redraw: stroke graphic");
      }
    }, // redraw
});
