// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

        // progress bar ui element
        progress_bar: {
          default: null,
          type: cc.ProgressBar,
        },

        // the maximum limit
        max_value: 0,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
      cc.assert(this.progress_bar, "[progress bar] ui element is missing");
      // set the value to the max
      this.value = this.max_value;
    },



    update (dt) {
      this.value += dt;
      //cc.log("[HealthBar] this.value: " + this.value);

      // reset the current value, when it reaches the maximum
      if (this.value == this.max_value) {
          this.value = 0;
      }

      var percentage = 0;
      if(0 != this.value)
         percentage = this.value / this.max_value;

      // update the ui
      this.progress_bar.progress = percentage;
    },
});
