// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    // update (dt) {},

    sliderEvent (eventData) {
      cc.log("[SliderScript] sliderEvent eventData: " + eventData);
      cc.log("[SliderScript] sliderEvent eventData.name: " + eventData.name);
      cc.log("[SliderScript] sliderEvent eventData.progress: " + eventData.progress);


      if(cc.Slider.Direction.Horizontal === eventData.direction)
        cc.log("[SliderScript] sliderEvent cc.Slider.Direction.Horizontal");
      else if (cc.Slider.Direction.Vertical === eventData.direction)
        cc.log("[SliderScript] sliderEvent cc.Slider.Direction.Vertical");



      // cc.log("[SliderScript] sliderEvent eventData.string: " + eventData.string);
      // cc.log("[SliderScript] sliderEvent eventData.node: " + eventData.node);
       cc.log("[EditBoxScript] sliderEvent eventData.node.name: " + eventData.node.name);

      // print all the keys in the object
      // console.log(Object.keys(eventData));
      // console.log(Object.keys(eventData.node));
    },
});
