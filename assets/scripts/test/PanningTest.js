/**
 * Reference:
 * http://www.cocos2d-x.org/docs/api-ref/creator/v1.8/en/classes/Event.EventTouch.html
 */

cc.Class({
    extends: cc.Component,

    properties: {
      target_node: {
        default: null,
        type: cc.Node,
      },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
      var canvas = cc.find('Canvas');
      canvas.on(cc.Node.EventType.TOUCH_START, this.onTouchBegan, this);
      canvas.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMoved, this);
      canvas.on(cc.Node.EventType.TOUCH_END, this.onTouchEnded, this);
      canvas.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCanceled, this);
    },

    start: function () {

    }, // start

    // update (dt) {},

    // [Touch] input handler
    /**
     * @param   event             Event.EventTouch class object
     */
    onTouchBegan: function (event) {
      // Returns touch location
      var touchLocation = event.touch.getLocation();
      // Returns the current touch location in screen coordinates
      var touchViewLocation = event.touch.getLocationInView();
      // Returns the previous touch location
      var touchPreviousLocation = event.touch.getPreviousLocation();
      // Returns the start touch location
      var touchStartLocation = event.touch.getStartLocation();
      // Returns the delta distance from the previous location to current location.
      var touchDelta = event.touch.getDelta();

      // cc.log("[PanningTest] onTouchBegan touchLocation: {" + touchLocation.x + ", " + touchLocation.y + "}");
      // cc.log("[PanningTest] onTouchBegan touchViewLocation: {" + touchViewLocation.x + ", " + touchViewLocation.y + "}");
      // cc.log("[PanningTest] onTouchBegan touchPreviousLocation: {" + touchPreviousLocation.x + ", " + touchPreviousLocation.y + "}");
      // cc.log("[PanningTest] onTouchBegan touchStartLocation: {" + touchStartLocation.x + ", " + touchStartLocation.y + "}");
      // cc.log("[PanningTest] onTouchBegan touchDelta: {" + touchDelta.x + ", " + touchDelta.y + "}");
      // cc.log("[PanningTest] onTouchBegan ENDS");

    }, // onTouchBegan

    /**
     * @param   event             Event.EventTouch class object
     */
    onTouchMoved: function (event) {
      // Returns touch location
      var touchLocation = event.touch.getLocation();
      // Returns the current touch location in screen coordinates
      var touchViewLocation = event.touch.getLocationInView();
      // Returns the previous touch location
      var touchPreviousLocation = event.touch.getPreviousLocation();
      // Returns the start touch location
      var touchStartLocation = event.touch.getStartLocation();
      // Returns the delta distance from the previous location to current location.
      var touchDelta = event.touch.getDelta();

      // cc.log("[PanningTest] onTouchMoved touchLocation: {" + touchLocation.x + ", " + touchLocation.y + "}");
      // cc.log("[PanningTest] onTouchMoved touchViewLocation: {" + touchViewLocation.x + ", " + touchViewLocation.y + "}");
      // cc.log("[PanningTest] onTouchMoved touchPreviousLocation: {" + touchPreviousLocation.x + ", " + touchPreviousLocation.y + "}");
      // cc.log("[PanningTest] onTouchMoved touchStartLocation: {" + touchStartLocation.x + ", " + touchStartLocation.y + "}");
      // cc.log("[PanningTest] onTouchMoved touchDelta: {" + touchDelta.x + ", " + touchDelta.y + "}");
      // cc.log("[PanningTest] onTouchMoved ENDS");

      if(null !== this.target_node) {
        //cc.log("[PanningTest] onTouchMoved this.target_node b4: {" + this.target_node.x + ", " + this.target_node.y + "}");
        this.target_node.x += touchDelta.x;
        this.target_node.y += touchDelta.y;
        //cc.log("[PanningTest] onTouchMoved this.target_node after: {" + this.target_node.x + ", " + this.target_node.y + "}");
      }

    }, // onTouchMoved

    /**
     * @param   event             Event.EventTouch class object
     */
    onTouchEnded: function (event) {
      // Returns touch location
      var touchLocation = event.touch.getLocation();
      // Returns the current touch location in screen coordinates
      var touchViewLocation = event.touch.getLocationInView();
      // Returns the previous touch location
      var touchPreviousLocation = event.touch.getPreviousLocation();
      // Returns the start touch location
      var touchStartLocation = event.touch.getStartLocation();
      // Returns the delta distance from the previous location to current location.
      var touchDelta = event.touch.getDelta();

      // cc.log("[PanningTest] onTouchEnded touchLocation: {" + touchLocation.x + ", " + touchLocation.y + "}");
      // cc.log("[PanningTest] onTouchEnded touchViewLocation: {" + touchViewLocation.x + ", " + touchViewLocation.y + "}");
      // cc.log("[PanningTest] onTouchEnded touchPreviousLocation: {" + touchPreviousLocation.x + ", " + touchPreviousLocation.y + "}");
      // cc.log("[PanningTest] onTouchEnded touchStartLocation: {" + touchStartLocation.x + ", " + touchStartLocation.y + "}");
      // cc.log("[PanningTest] onTouchEnded touchDelta: {" + touchDelta.x + ", " + touchDelta.y + "}");
      // cc.log("[PanningTest] onTouchEnded ENDS");

    }, // onTouchEnded

    /**
     * @param   event             Event.EventTouch class object
     */
    onTouchCanceled: function (event) {
      // Returns touch location
      var touchLocation = event.touch.getLocation();
      // Returns the current touch location in screen coordinates
      var touchViewLocation = event.touch.getLocationInView();
      // Returns the previous touch location
      var touchPreviousLocation = event.touch.getPreviousLocation();
      // Returns the start touch location
      var touchStartLocation = event.touch.getStartLocation();
      // Returns the delta distance from the previous location to current location.
      var touchDelta = event.touch.getDelta();

      // cc.log("[PanningTest] onTouchCanceled touchLocation: {" + touchLocation.x + ", " + touchLocation.y + "}");
      // cc.log("[PanningTest] onTouchCanceled touchViewLocation: {" + touchViewLocation.x + ", " + touchViewLocation.y + "}");
      // cc.log("[PanningTest] onTouchCanceled touchPreviousLocation: {" + touchPreviousLocation.x + ", " + touchPreviousLocation.y + "}");
      // cc.log("[PanningTest] onTouchCanceled touchStartLocation: {" + touchStartLocation.x + ", " + touchStartLocation.y + "}");
      // cc.log("[PanningTest] onTouchCanceled touchDelta: {" + touchDelta.x + ", " + touchDelta.y + "}");
      // cc.log("[PanningTest] onTouchCanceled ENDS");

    }, // onTouchCanceled
});
