/**
 * Reference:
 * https://github.com/cocos-creator/creator-docs/blob/master/en/scripting/internal-events.md
 */

const PathFinding = require('../PathFinding');

cc.Class({
    extends: cc.Component,

    properties: {
        debug_draw_grid: {
          default: null,
          //type: cc.Component,
          type: require('../ui/DebugDrawGrid'),
        },

        debug_draw_path: {
          default: null,
          //type: cc.Component,
          type: require('../ui/DebugDrawPath'),
        },

        floorLayerName: {
            default: 'floor'
        },

        barrierLayerName: {
            default: 'barrier'
        },

        objectGroupName: {
            default: 'players'
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
      // get the tiled map object reference
      this.tiledMap = this.node.getComponent('cc.TiledMap');
      cc.assert(undefined !== this.tiledMap || null !== this.tiledMap, "TiledMap component not found in this node");

      var mapSize = this.tiledMap.getMapSize();
      var tileSize =this.tiledMap.getTileSize();
      this.tiledMap.getMapOrientation(); // TiledMap.Orientation

      this.layerFloor = this.tiledMap.getLayer(this.floorLayerName);
      cc.assert(undefined !== this.layerFloor || null !== this.layerFloor, `[${this.floorLayerName}] layer not found in TiledMap`);

      this.layerBarrier = this.tiledMap.getLayer(this.barrierLayerName);
      cc.assert(undefined !== this.layerBarrier || null !== layerBarrier.layerFloor, `[${this.barrierLayerName}] layer not found in TiledMap`);

////////////////////////////////////////////////////////////////////////////////
var objectGroup = this.tiledMap.getObjectGroup("players");
if (objectGroup) {
  var startObj = objectGroup.getObject("SpawnPoint");
  var endObj = objectGroup.getObject("SuccessPoint");
  if (startObj && endObj) {
    // get the position of the objects
    this.startPixelPos = cc.p(startObj.sgNode.x, startObj.sgNode.y);
    this.endPixelPos = cc.p(endObj.sgNode.x, endObj.sgNode.y);

    // convert the object position to tile position
    this.startGridPos = cc.p(this.startPixelPos.x / tileSize.width, this.startPixelPos.y / tileSize.height);
    this.endGridPos = cc.p(this.endPixelPos.x / tileSize.width, this.endPixelPos.y / tileSize.height);

    // reverse the y-axis
    this.startGridPos.y = Math.abs(this.startGridPos.y - mapSize.height) - 1;
    this.endGridPos.y = Math.abs(this.endGridPos.y - mapSize.height) - 1;

    // convert the pixel position to the world space
    this.startPixelPos = this.tiledMap.node.parent.convertToWorldSpace(this.startPixelPos);
    this.endPixelPos = this.tiledMap.node.parent.convertToWorldSpace(this.endPixelPos);
  }
}
// console.info("this.startPixelPos");console.info(this.startPixelPos);
// console.info("this.endPixelPos");console.info(this.endPixelPos);
// console.info("this.gridPosStart");console.info(this.gridPosStart);
// console.info("this.gridPosEnd");console.info(this.gridPosEnd);
////////////////////////////////////////////////////////////////////////////////

      // set the map and tile size
      // cc.log("Map Size: " + mapSize);
      // cc.log("Tile Size: " + tileSize);
      this.debug_draw_grid.map_size = mapSize;
      this.debug_draw_grid.tile_size = tileSize;
      // set the debug node position offset
      this.debug_draw_grid.node.position = new cc.Vec2(this.node.width / 2 * -1, this.node.height / 2 * -1);

      /////////////////////////////////////////////////////////////////////////////////
      var graph = [];
      // initialize the graph
      PathFinding.fill2DimensionsArray(graph, mapSize.width, mapSize.height);
      /////////////////////////////////////////////////////////////////////////////////


      var blockedTiles = [];
      for (var y = 0; y < mapSize.height; y++) {
        for (var x = 0; x < mapSize.width; x++) {
          var pos = new cc.Vec2(x, y);

          var tileId = this.layerBarrier.getTileGIDAt(pos);
          var tileSprite = this.layerBarrier.getTileAt(pos);
          if (tileId) {
            blockedTiles.push(pos);
            /////////////////////////////////////////////////////////////////////////////////
            graph[x][y] = 1;
            /////////////////////////////////////////////////////////////////////////////////
          }
        } // x-axis
      } // y-axis
      // cc.log('blockedTiles!');
      // console.log(blockedTiles);
      // console.log("this.debug_draw_grid");
      // console.log(this.debug_draw_grid);

      // set the block tile positions
      this.debug_draw_grid.blockedTilePositions = blockedTiles;

      /////////////////////////////////////////////////////////////////////////////////
      // debug print all the content in this 2d array,
      // used to display the graph weights
      //console.info(graph);
      // set to local properties
      this.graph = graph;
      /////////////////////////////////////////////////////////////////////////////////
      var pathStart = cc.p(4, 28); // start position
      var pathEnd = cc.p(20, 2); // end position
      this.startGridPos = pathStart;
      this.endGridPos = pathEnd;
      this.updateRoute();
      /////////////////////////////////////////////////////////////////////////////////

      var canvas = cc.find('Canvas');
      canvas.on(cc.Node.EventType.MOUSE_DOWN, this.onMouseDown, this);
      canvas.on(cc.Node.EventType.MOUSE_UP, this.onMouseUp, this);
      canvas.on(cc.Node.EventType.MOUSE_WHEEL, this.onMouseWheel, this);
    }, // onLoad

    onMouseDown: function (event) {
      console.info("[TiledMapGraph] onMouseDown");
      console.info(event);

      var tiledMap = this.tiledMap;
      var touchLoc = event.getLocation();
      cc.log("[TiledMapGraph] onMouseDown touchLoc: {" + touchLoc.x + ", " + touchLoc.y + "}");
      // convert the touch position to tiled map node position
      var nodePos = tiledMap.node.convertToNodeSpace(touchLoc);
      cc.log("[TiledMapGraph] onMouseDown nodePos: {" + nodePos.x + ", " + nodePos.y + "}");

      // calculate the tile position
      var tilePos = new cc.Vec2(Math.trunc(nodePos.x / tiledMap.getTileSize().width),
                                Math.trunc(nodePos.y / tiledMap.getTileSize().height));
      // reverse the y-axis
      tilePos.y = Math.abs(tilePos.y - tiledMap.getMapSize().height) - 1;
      cc.log("[TiledMapGraph] onMouseDown tilePos: {" + tilePos.x + ", " + tilePos.y + "}");

      // check which button is pressed
      switch (event.getButton()) {
        case cc.Event.EventMouse.BUTTON_LEFT:
          console.log("Left mouse button pressed");
          if(0 <= tilePos.x && tilePos.x < tiledMap.getMapSize().width &&
             0 <= tilePos.y && tilePos.y < tiledMap.getMapSize().height)
           {
             this.startGridPos = tilePos;
             cc.log("[TiledMapGraph] onMouseDown this.startGridPos: {" + this.startGridPos.x + ", " + this.startGridPos.y + "}");
             this.updateRoute();

             // redraw the path
             if(this.debug_draw_path)
              this.debug_draw_path.redraw();
           }
          break;
        case cc.Event.EventMouse.BUTTON_RIGHT:
          console.log("Right mouse button pressed");
          if(0 <= tilePos.x && tilePos.x < tiledMap.getMapSize().width &&
             0 <= tilePos.y && tilePos.y < tiledMap.getMapSize().height)
           {
             this.endGridPos = tilePos;
             cc.log("[TiledMapGraph] onMouseDown this.startGridPos: {" + this.endGridPos.x + ", " + this.endGridPos.y + "}");
             this.updateRoute();

             // redraw the path
             if(this.debug_draw_path)
              this.debug_draw_path.redraw();
           }
          break;
        case cc.Event.EventMouse.BUTTON_MIDDLE:
          console.log("Middle mouse button pressed");
          break;
        default:
          console.log("Unsupported mouse button pressed");
          break;
      }
    }, // onMouseDown

    onMouseUp: function (event) {
      // console.info("[TiledMapGraph] onMouseUp");
      // console.info(event);

      // check which button is released
      switch (event.getButton()) {
        case cc.Event.EventMouse.BUTTON_LEFT:
          console.log("Left mouse button released");
          break;
        case cc.Event.EventMouse.BUTTON_RIGHT:
          console.log("Right mouse button released");
          break;
        case cc.Event.EventMouse.BUTTON_MIDDLE:
          console.log("Middle mouse button released");
          break;
        default:
          console.log("Unsupported mouse button released");
          break;
      }
    }, // onMouseUp

    onMouseWheel: function (event) {
      console.info("[TiledMapGraph] onMouseWheel");
      console.info(event);
    }, // onMouseWheel

    start: function () {

//       // the world grid: a 2d array of tiles
//       var world = [[]];
//
//       // size in the world in sprite tiles
//       var worldWidth = 16;
//       var worldHeight = 16;
//
//
//       // start and end of path
//       var pathStart = [worldWidth,worldHeight];
//       var pathEnd = [0,0];
//       var currentPath = [];
//
//
//
// //      console.log('Creating world...');
//
//       	// create emptiness
//       	for (var x=0; x < worldWidth; x++)
//       	{
//       		world[x] = [];
//
//       		for (var y=0; y < worldHeight; y++)
//       		{
//       			world[x][y] = 0;
//       		}
//       	}
//
//       	// scatter some walls
//       	for (var x=0; x < worldWidth; x++)
//       	{
//       		for (var y=0; y < worldHeight; y++)
//       		{
//       			if (Math.random() > 0.75)
//       			world[x][y] = 1;
//       		}
//       	}
//         console.log('world...');
//         console.log(world);
//
//       	// calculate initial possible path
//       	// note: unlikely but possible to never find one...
//       	currentPath = [];
//       	while (currentPath.length == 0)
//       	{
//       		pathStart = [Math.floor(Math.random()*worldWidth),Math.floor(Math.random()*worldHeight)];
//       		pathEnd = [Math.floor(Math.random()*worldWidth),Math.floor(Math.random()*worldHeight)];
//           console.log(pathStart);
//           console.log(pathEnd);
//
//       		if (world[pathStart[0]][pathStart[1]] == 0)
//       		currentPath = PathFinding.findPath(world,pathStart,pathEnd);
//       	}
//         console.info("currentPath");
//         console.info(currentPath);

      //PathFinding.findPath();

    }, // start

    update: function (dt) {
    }, // update

    updateRoute: function() {
      var pathStart = [this.startGridPos.x, this.startGridPos.y];
      var pathEnd = [this.endGridPos.x, this.endGridPos.y];
      var graph = this.graph;
      var mapSize = this.tiledMap.getMapSize();
      var tileSize =this.tiledMap.getTileSize();

      console.info(this.startGridPos);
      console.info(this.endGridPos);
      console.info(pathStart);
      console.info(pathEnd);
      var currentPath = PathFinding.findPath(graph, pathStart, pathEnd);
      console.info(currentPath);

      var currentPathPixelPositions = [];
      for (let i = 0; i < currentPath.length; i++) {
        let x = currentPath[i][0];
        let y = currentPath[i][1];

        let position = cc.p(x * tileSize.width + (tileSize.width * 0.5), y * tileSize.height + (tileSize.height * 0.5));
        // reverse the y-axis
        position.y = Math.abs((y * tileSize.height) - (mapSize.height * tileSize.height)) - tileSize.height + (tileSize.height * 0.5);
        currentPathPixelPositions.push(position);
      }
      console.info(currentPathPixelPositions);
      /////////////////////////////////////////////////////////////////////////////////
      if (this.debug_draw_path) {
        // set the array of points to draw
        this.debug_draw_path.path = currentPathPixelPositions; //[this.startPixelPos, this.endPixelPos];
        // set the draw offset
        this.debug_draw_path.offset = cc.p(mapSize.width * tileSize.width * -0.5,
                                           mapSize.height * tileSize.height * -0.5);
        // console.info("[TiledMapGraph] this.debug_draw_path.path");
        // console.info(this.debug_draw_path.path);
      }
      /////////////////////////////////////////////////////////////////////////////////
    }, // updateRoute
});
