// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },


        // waypoints
        waypoints: {
          default: [],
          type: cc.Node,
        },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
      // convert all the waypoint nodes to world position
      var worldPosition = [];

      // loop through all the waypoints array
      for (var i = 0; i < this.waypoints.length; i++) {
        var waypointNode = this.waypoints[i];
        cc.assert(waypointNode, "null node at index:" + i);

        // convert the node position to world position
        var waypointParentNode = waypointNode.parent;
        var waypointWorldPos = waypointParentNode.convertToWorldSpaceAR(waypointNode.position);

        // add the position vector to the back of the array
        worldPosition.push(waypointWorldPos);

        console.info("[WaypointTest] start waypoint[" + i + "]: {" + waypointNode.position.x + ", " + waypointNode.position.y + "}");
      }


      // get [PatrolController] component.
      var patrolScript = this.node.getComponent("PatrolController");
      if(null !== patrolScript) {
        patrolScript.routes = worldPosition;
        console.info("[WaypointTest] start: set the patrol waypoint positions in the patrol script");
        patrolScript.begin();
        console.info("[WaypointTest] start: patrolScript.begin();");
      }
    },
});
