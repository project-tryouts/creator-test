
cc.Class({
    extends: cc.Component,

    properties: {
      force: {
        default: 1,
        tooltip: 'the force to move this object in the gravity direction',
      }, // force
      direction: {
        default: cc.Vec2.ZERO,
        tooltip: 'the gravity direction',
      }, // direction
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start: function () {
      // console.log("this.direction: {" + this.direction.x + "," + this.direction.y + "}");
      // console.log(this.direction);

      this.node.on("gravity_off", this.disableGravity, this);
      this.node.on("gravity_on", this.enableGravity, this);
    }, // start

    update: function (dt) {
      let forceVector = Object.create(this.direction);
      forceVector.mulSelf(this.force * dt);
      this.node.position = this.node.position.addSelf(forceVector);
    }, // update


    disableGravity: function(event) {
      this.enabled = false;
      console.log("disableGravity");
      console.log(event);
    }, // disableGravity

    enableGravity: function(event) {
      this.enabled = true;
      console.log("enableGravity");
      console.log(event);
    }, // enableGravity
});
