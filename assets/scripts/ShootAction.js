// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

		projectile: {
			default: null,
			type: cc.Prefab
		},

		firingPosition:{
			default: cc.Vec2.ZERO,
		},
		firingDirection:{
			default: cc.Vec2.ZERO,
		},

		cooldownTimer: 1,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
      // this will reference the variable from the script,
      // which repesent a global variable.
      //this.timer = require("Timer");
    this.timer = Object.create(require("Timer"));

		this.timer.timerLimit = this.cooldownTimer;

		// ready the cooldown for the first time
		this.timer.update(this.timer.timerLimit);

    /////////////////////////////////////////////////
    // var timer2 = new Timer();
    // timer2.timerLimit = 35500;
    // cc.log("this.timer.timerLimit: " + this.timer.timerLimit);
    // cc.log("timer2.timerLimit: " + timer2.timerLimit);
    /////////////////////////////////////////////////
    // var newTimerScript = require("ccTimer");
    //
    // var newTimerObj1 = newTimerScript(1);
    // var newTimerObj2 = newTimerScript(2);
    // //newTimerObj1.timerElapsed += 1;
    // cc.log("newTimerObj1.timerElapsed: " + newTimerObj1.timerElapsed);
    // cc.log("newTimerObj2.timerElapsed: " + newTimerObj2.timerElapsed);
    // //newTimerObj.update(5);
    // //cc.log("newTimerObj.timerElapsed: " + newTimerObj.timerElapsed);
    //
    // //this.timerNew = new NewTimer();
    /////////////////////////////////////////////////
    // WORKING EXAMPLE
    // var timer2 = Object.create(this.timer);
    // timer2.timerLimit = 1000;
    // cc.log("timer2.timerLimit: " + timer2.timerLimit);
    //
    // var timer3 = Object.create(this.timer);
    // timer3.timerLimit = 2000;
    // cc.log("timer2.timerLimit: " + timer2.timerLimit);
    // cc.log("timer3.timerLimit: " + timer3.timerLimit);
    /////////////////////////////////////////////////
	},

    start: function () {

    },

    // called every frame
    update: function (dt) {
		this.timer.update(dt);

    // if(null !== this.timerNew)
    //   this.timerNew.timerElapsed += dt;


	},

	// return boolean
	isReady: function() {
		return this.timer.IsReady();
	},

	// return value between 0.0 -> 1.0
	getCooldownPercentage: function () {
		return this.timer.GetPercentage();
	},

	activate: function () {
		//cc.log("ShootAction.activate");

		// reset the timer
		this.timer.reset();

		// find the position of this node in world space
		var parentNode = this.node.parent;

		var worldPos = parentNode.convertToWorldSpaceAR(this.node.position);


		////////////////////////////////////////////////////////////////
		// center of the screen
		this.firingPosition.x = cc.director.getWinSize().width * 0.5;
		this.firingPosition.y = cc.director.getWinSize().height * 0.5;

		var randomNumber = (cc.rand() % 100 )* ((cc.rand() % 1)? -1:1);
		cc.log("random number: " + randomNumber);

		this.firingPosition.x += randomNumber;
		this.firingPosition.y += randomNumber;
		////////////////////////////////////////////////////////////////
		this.firingPosition = worldPos;
		////////////////////////////////////////////////////////////////

		// get the active current scene
		var currentScene = cc.director.getScene();
		cc.assert(currentScene, "No scene detected by [cc.director]");

		// create the prefab instance
		var projectileObject = cc.instantiate(this.projectile);
		////////////////////////////////////////////////////////////////
		var bulletControl = projectileObject.getComponent("BulletController");
		// 22/01: Dont know why it cannot set cc.Vec2, it will give runtime error of .x not found in the other script.
		//bulletControl.facingDirection = cc.Vec2(0.5, -1);

		//bulletControl.facingDirection.x = 0.5;
		//bulletControl.facingDirection.y = 1;
		////////////////////////////////////////////////////////////////
    bulletControl.facingDirection = this.firingDirection;



		// set the object at the start position
		projectileObject.x = this.firingPosition.x;
		projectileObject.y = this.firingPosition.y;

		// add to the current scene
		currentScene.addChild(projectileObject);

		// make it move towards a direction
		this.firingDirection;
	}, // activate [function]
});
