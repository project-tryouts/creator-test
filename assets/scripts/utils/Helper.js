/**
 @param     decimal            integer, decimal form
 @param     n       			     integer, number of decmical places???
 @reference
 https://stackoverflow.com/a/49862603/2531281
 */
export function trunc(decimal, n=2){
  let x = decimal + ''; // string
  return x.lastIndexOf('.')>=0?parseFloat(x.substr(0,x.lastIndexOf('.')+(n+1))):decimal; // You can use indexOf() instead of lastIndexOf()
} // trunc
