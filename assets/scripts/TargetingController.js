// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

        target : {
          default: null,
          type: cc.Node,
        },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    update (dt) {
      if(null !== this.target) {
        var thisParentNode = this.node.parent;
        var thisWorldPos = thisParentNode.convertToWorldSpaceAR(this.node.position);

        var targetParentNode = this.target.parent;
        var targetWorldPos = targetParentNode.convertToWorldSpaceAR(this.target.position);
        var directionVector = targetWorldPos.sub(thisWorldPos);

        // use the vector{0,1}, because it uses trigo to calculate the angle.
        var origin = cc.Vec2.UP;
        // add a slight offset to the destination
        var offset = directionVector.mul(1);
        var targetPostition = origin.add(offset);
        // calculate the signed angle, so that the angle will be calculated properly based on the 360 degree.
        // if its not signed, it will return a degree between 0 to 180.
        var radianAngle = origin.signAngle (targetPostition);
        var degreeAngle = cc.radiansToDegrees(radianAngle);


        this.node.rotation = degreeAngle;
      }
    },
});
