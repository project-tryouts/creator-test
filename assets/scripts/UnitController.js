// Learn cc.Class:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/class/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

    		// the player controller script
    		player: {
    			default: null,
    			type: cc.Component,
    		},

        // facing
        facing_direction: {
          default: cc.Vec2.ZERO,
        },

    		// the unit moment speed
    		move_speed: 10,
        rotation_speed: 10,
    		// move direction
    		move_direction: {
    			default: cc.Vec2.ZERO,
    		},

        // rotation
        rotation_direction: 0,
    },


	// ShootAction [Script]
	scriptA: null,

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
		// script for [actionA]
		this.scriptA = this.getComponent("ShootAction");
		cc.assert(this.scriptA, "Script for [ActionA]");
	}, // onLoad [function]

    start: function () {
		cc.log("start");
    }, // start [function]

    // called every frame
    update: function (dt) {
        //this.sheep.x += this.move_speed * dt;

		// check the player controller script for the input
		if(this.player)
		{
			// reset the value
			this.move_direction = cc.Vec2.ZERO;

			// check if the input state is still active
			// // X-axis //
			// if ( this.player.directionLeft )
			// 	this.move_direction.x = -1;
			// else if( this.player.directionRight )
			// 	this.move_direction.x = 1;
			// // Y-axis //
			// if ( this.player.directionUp )
			// 	this.move_direction.y = 1;
			// else if( this.player.directionDown )
			// 	this.move_direction.y = -1;

      // rotation //
      if ( this.player.directionLeft )
        this.rotation_direction = -1;
      else if( this.player.directionRight )
        this.rotation_direction = 1;
      else
        this.rotation_direction = 0;


        var firingDirection = cc.Vec2.ZERO;
        // find the firing direction
  			if ( 0 != this.player.touchPosition.x && 0 != this.player.touchPosition.y )
  			{
          var touchPos = new cc.Vec2(this.player.touchPosition.x, this.player.touchPosition.y);
  				//cc.log("[UnitController] update touch position: {" + touchPos.x + ", " + touchPos.y + "}");


          // find the position of this node in world space
      		var parentNode = this.node.parent;
      		var worldPos = parentNode.convertToWorldSpaceAR(this.node.position);
  				//cc.log("[UnitController] update node world position: {" + worldPos.x + ", " + worldPos.y + "}");

          var directionVector = touchPos.sub(worldPos);
          directionVector.normalizeSelf();
          firingDirection = directionVector;
          //cc.log("[UnitController] update directionVector: {" + directionVector.x + ", " + directionVector.y + "}");

          var radian = worldPos.angle(touchPos);
          var degree = cc.radiansToDegrees(radian);
          //cc.log("[UnitController] update directionVector angle radian: {" + radian + "}");
          //cc.log("[UnitController] update directionVector angle degree: {" + degree + "}");
  			} // find the firing direction
			else
        var fireActionTrigger = false;
				//firingDirection.y = 1;


        if ( this.player.touchAction )
  			{
          fireActionTrigger = true;
          // set the firing direction based on the touch position relative from the object
          this.scriptA.firingDirection = firingDirection;
          //cc.log("[UnitController] update this.scriptA.facingDirection: {" + this.scriptA.firingDirection.x + ", " + this.scriptA.firingDirection.y + "}")
  			} else if( this.player.actionA ) {
          fireActionTrigger = true;
          // set the firing direction based on the object facing direction
          ////////////////////////////////////////////////////////////////////////////
          var facing = cc.Vec2.UP;
          cc.log("[UnitController] facing UP: {" + facing.x + ", " + facing.y + "}");
          var degreeRadian = cc.degreesToRadians(this.node.rotation) * -1;
          facing = facing.rotate(degreeRadian);
          cc.log("[UnitController] rotation: {" + this.node.rotation + "}");
          cc.log("[UnitController] degreeRadian: {" + degreeRadian + "}");
          cc.log("[UnitController] facing: {" + facing.x + ", " + facing.y + "}");
          this.scriptA.firingDirection = facing;
          ////////////////////////////////////////////////////////////////////////////
        }

        if(fireActionTrigger) {
          //cc.log("[UnitController] firingDirection: {" + this.scriptA.firingDirection.x + ", " + this.scriptA.firingDirection.y + "}");

          if(this.scriptA.isReady())
            this.scriptA.activate();
          else
            console.log("action not ready, " + this.scriptA.getCooldownPercentage());
        }
		} // player script check


		// move this node
		if( 0 != this.move_speed )
		{
			if( 0 != this.move_direction.x )
				this.node.x += this.move_direction.x * this.move_speed * dt;
			if( 0 != this.move_direction.y )
				this.node.y += this.move_direction.y * this.move_speed * dt;
		}

    ////////////////////////////////////////////////////////////////////////////
    if( 0 != this.rotation_speed && 0 != this.rotation_direction )
      this.node.rotation += this.rotation_direction * this.rotation_speed * dt;
    ////////////////////////////////////////////////////////////////////////////


    //cc.log("[UnitController] facing_direction: {" + this.facing_direction.x + ", " + this.facing_direction.y + "}");

		// actions

		//cc.log(this.node.position);
		//cc.log("update: " + dt);
    },

	// input commands
    turnLeft: function () {
        //this.move_speed = -100;
        //this.sheep.scaleX = 1;
    },

    turnRight: function () {
        //this.move_speed = 100;
        //this.sheep.scaleX = -1;
    }
});
